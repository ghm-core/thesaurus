//
//  ViewController.swift
//  Thesaurus
//
//  Created by VJ Pranay on 20/06/18.
//  Copyright © 2018 Lamda School. All rights reserved.
//

import UIKit

class ViewController: UIViewController {



    @IBAction func showSynonyms(_ sender: Any) {
        let UserText = userInput.text
        let FinalUserText = UserText?.lowercased()
        var answers = synonyms(word : FinalUserText!)
        if answers.count == 0 {
            resultText.text = "Sorry , Please try again"
        }
        else{
            var finalString : String = "Here are the synonyms for \(FinalUserText)\n"
            for word in answers{
                finalString += "\(word)\n"
            }
            resultText.text = finalString
        }
    }
    
    @IBOutlet weak var userInput: UITextField!
    @IBOutlet weak var resultText: UITextView!
}

