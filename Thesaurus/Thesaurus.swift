//
//  Thesaurus.swift
//  Thesaurus
//
//  Created by VJ Pranay on 20/06/18.
//  Copyright © 2018 Lamda School. All rights reserved.
//

import Foundation
let synonyms = ["swift" : ["abrupt", "expeditious", "hasty", "nimble", "quick", "rapid", "speedy", "sudden", "unexpected"],
                "objective" : ["detached", "disinterested", "dispassionate", "equitable", "evenhanded", "nonpartisan", "open-minded", "unbiased"],
                "calculate" : ["adjust", "appraise", "consider", "count", "determine", "forecast", "gauge", "guess", "measure", "multiply", "reckon", "subtract", "tally", "weigh", "work out"],
                "create" : ["build", "conceive", "constitute", "construct", "design", "devise", "discover", "establish", "forge", "form"]]
func synonyms(word: String) -> [String] {
    if let result: [String] = synonyms[word] {
        return result
    } else {
        
        let result = ["Sorry! Please try again"]
        return result
    }
}
